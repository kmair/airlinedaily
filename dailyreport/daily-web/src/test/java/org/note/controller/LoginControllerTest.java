package org.note.controller;

import static org.junit.Assert.*;

import org.daily.dao.hibernate.dao.EntityOperationImpl;
import org.daily.dao.hibernate.dao.EntityOperationInface;
import org.daily.validation.entity.mysql.Account;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class LoginControllerTest {
	ApplicationContext act;
	EntityOperationInface dao;

	@Before
	public void setUp() throws Exception {
		act = new ClassPathXmlApplicationContext("org/daily/web/resources/spring-controller.xml");
		dao = (EntityOperationInface) act.getBean("mySQLOperate");
	}

	// @Test
	public void testShowLogin() {
		fail("Not yet implemented");
	}

	// @Test
	public void testAccountShow() {
		fail("Not yet implemented");
	}

	// @Test
	public void testShowtemplate() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetEntity() {
		Account account = (Account) dao.getEntity(Account.class, 1);
		assertEquals("TOM", account.getUserName());
	}
}
