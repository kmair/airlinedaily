package org.note.controller;

import org.daily.dao.hibernate.dao.EntityOperationInface;
import org.daily.validation.entity.mysql.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/users")
public class UsersController {
	@Autowired
	@Qualifier("mySQLOperate")
	EntityOperationInface dao;

	@RequestMapping(value = { "/{userId}", "/{userId}/userhome" }, method = RequestMethod.GET)
	public String accountHome(@PathVariable String userId, Model model) {
		Account account = (Account) ((EntityOperationInface) dao).getOneEntity(Account.class, "userId", userId);
		// if("")
		model.addAttribute("account", account);
		return "userhome";
	}

}
