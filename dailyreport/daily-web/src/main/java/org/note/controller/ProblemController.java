package org.note.controller;

import java.util.List;

import org.daily.dao.hibernate.dao.EntityOperationInface;
import org.daily.note.entity.mysql.Problems;
import org.daily.validation.entity.mysql.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("**/{userId}/problems")
public class ProblemController {
	private Problems problem;
	private EntityOperationInface dao;

	@Autowired
	public ProblemController(Problems problem, @Qualifier("mySQLOperate")EntityOperationInface dao) {
		this.problem = problem;
		this.dao = dao;
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(method = RequestMethod.GET)
	public String getProblem(@PathVariable String userId, Model model) {
		List<Problems> problemList = (List<Problems>) dao.getAllEntity(Problems.class);
		model.addAttribute("problemList", problemList);
		return "problems";
	}

	@RequestMapping(method = RequestMethod.PUT)
	public void putProblem() {
	}

	@RequestMapping(method = RequestMethod.DELETE)
	public String deleteProblem() {
		return null;
	}

	@RequestMapping(value = "/creat", method = RequestMethod.GET)
	public String problemsCreat(Model model) {
		model.addAttribute("problem", problem);
		return "/problems/creat";
	}

	@RequestMapping(value = "/creat", method = RequestMethod.POST)
	// @ResponseStatus(HttpStatus.CREATED)
	public String creatProblem(Problems problem, Model model, Account account) {
		dao.SaveEntity(problem);
		return "redirect:users/" + account.getUserId() + "/problems";
	}
}
