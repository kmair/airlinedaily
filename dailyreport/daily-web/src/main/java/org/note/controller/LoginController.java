package org.note.controller;

import javax.validation.Valid;

import org.daily.validation.check.DataOperateService;
import org.daily.validation.check.UserCheck;
import org.daily.validation.entity.mysql.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/")
public class LoginController {
	private Account account;

	private DataOperateService dataService;

	public LoginController() {
	}

	@Autowired
	public LoginController(Account account, @Qualifier("userCheck") DataOperateService dataService) {
		this.account = account;
		this.dataService = dataService;
	}

	@RequestMapping(value = { "/", "/index" }, method = RequestMethod.GET)
	public String showLogin(Model model) {
		model.addAttribute("account", account);
		return "signin";
	}

	/* 验证用户并转到用户界面 */
	@RequestMapping(value = "/usercheck", method = RequestMethod.POST)
	public String accountShow(@Valid Account account, BindingResult bindingResult, Model model) {
		UserCheck check = (UserCheck) dataService;
		if (bindingResult.hasErrors()) {
			return "signin";
		}
		boolean bl = check.check(Account.class, "userId", account.getUserId(), account.getPassWord());
		if (bl) {
			return "redirect:users/" + account.getUserId();
		} else {
			return "error";
		}
	}

	@RequestMapping(value = "**/template", method = RequestMethod.GET)
	public String showtemplate() {
		return "template";
	}
}
