<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<!DOCTYPE HTML">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>新建问题</title>
</head>
<body>
	<sf:form name="f" method="post" modelAttribute="paymentPlan">
		<fieldset>
			<h6>新建付款</h6>
			<sf:errors path="*" class="error" cssStyle="color:red" />
			<div>
				目录价：
				<sf:input path="aircraftBasicPrice" id="aircraftBasicPrice" placeholder="问题类型" />
			</div>
			<div>
				到场时间：
				<sf: path="deliveryDate" id="deliveryDate" placeholder="问题描述" />
			</div>
			<div>
				合同时间：
				<sf:input path="contractDate" id="contractDate" placeholder="请输入工号" />
			</div>
			<div>
				交付价：
				<sf:input path="aircraftActualPrice" id="aircraftActualPrice" placeholder="请输入工号" />
			</div>
			<div>
			<%-- 	<sf:hidden path="account" id="account" value=${account }/> --%>
			</div>
			<input class="btn-glow primary login" type="submit" value="提交" />
		</fieldset>
	</sf:form>
</body>
</html>