<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html>
<html>
<!-- bootstrap -->
<link href="/daily-web/resources/css/bootstrap/bootstrap.css"
	rel="stylesheet" />
<link href="/daily-web/resources/css/bootstrap/bootstrap-responsive.css"
	rel="stylesheet" />
<link href="/daily-web/resources/css/bootstrap/bootstrap-overrides.css"
	type="text/css" rel="stylesheet" />

<!-- libraries -->
<link href="/daily-web/resources/css/lib/jquery-ui-1.10.2.custom.css"
	rel="stylesheet" type="text/css" />
<link href="/daily-web/resources/css/lib/font-awesome.css"
	type="text/css" rel="stylesheet" />

<!-- global styles -->
<link rel="stylesheet" type="text/css"
	href="/daily-web/resources/css/layout.css" />
<link rel="stylesheet" type="text/css"
	href="/daily-web/resources/css/elements.css" />
<link rel="stylesheet" type="text/css"
	href="/daily-web/resources/css/icons.css" />

<head>
<%-- <title><tiles:getAsString name="title" /></title> --%>
</head>
<body>
	<tiles:insertAttribute name="header" />
	<tiles:insertAttribute name="menu" />
	<tiles:insertAttribute name="body" />
	<table>
		 <tr>
			<%-- <td><tiles:insertAttribute name="menu" /></td>
			<td><tiles:insertAttribute name="body" /></td> --%>
		</tr>
		<tr>
			<%-- <td colspan="2"><tiles:insertAttribute name="footer" /></td> --%>
		</tr> 
	</table>
	
</body>
</html>