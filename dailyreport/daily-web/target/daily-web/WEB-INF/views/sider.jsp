<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<!DOCTYPE html>
<html>
<head>

<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<!-- this page specific styles -->
<link rel="stylesheet"
	href="/daily-web/resources/css/compiled/index.css" type="text/css"
	media="screen" />

<!-- open sans font -->
<link
	href='http://fonts.useso.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800'
	rel='stylesheet' type='text/css' />

<!-- lato font -->
<link
	href='http://fonts.useso.com/css?family=Lato:300,400,700,900,300italic,400italic,700italic,900italic'
	rel='stylesheet' type='text/css' />

<!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
	<!-- sidebar -->
	<div id="sidebar-nav">
		<ul id="dashboard-menu">
			<li class="active">
				<div class="pointer">
					<div class="arrow"></div>
					<div class="arrow_border"></div>
				</div> <a href="userhome"> <i class="icon-home"></i> <span>主页</span>
			</a>
			</li>
			<li><a href="problems"> <i class="icon-book"></i>
					<span>运维日志</span>
			</a></li>
			<li><a class="dropdown-toggle" href="#"> <i
					class="icon-plane"></i> <span>预测</span> <i
					class="icon-chevron-down"></i>
			</a>
				<ul class="submenu">
					<li><a href="payment-plan">资金</a></li>
					<li><a href="#">税费</a></li>
					<li><a href="#">生产</a></li>
					<li><a href="#">成本</a></li>
				</ul></li>
		<!-- 	<li><a href="weixin"> <i class="icon-comments"></i> <span>Ã¥Â¾Â®Ã¤Â¿Â¡</span>
			</a></li>

			<li><a class="dropdown-toggle" href="#"> <i
					class="icon-edit"></i> <span>iuh</span> <i
					class="icon-chevron-down"></i>
			</a>
				<ul class="submenu">
					<li><a href="">Form showcase</a></li>
					<li><a href="form-wizard.html">Form wizard</a></li>
				</ul></li> -->

			<!--    <li>
                <a href="calendar.html">
                    <i class="icon-calendar-empty"></i>
                    <span>Calendar</span>
                </a>
            </li>
            <li>
                <a href="tables.html">
                    <i class="icon-th-large"></i>
                    <span>Tables</span>
                </a>
            </li>
            <li>
                <a class="dropdown-toggle ui-elements" href="#">
                    <i class="icon-code-fork"></i>
                    <span>UI Elements</span>
                    <i class="icon-chevron-down"></i>
                </a>
                <ul class="submenu">
                    <li><a href="ui-elements.html">UI Elements</a></li>
                    <li><a href="icons.html">Icons</a></li>
                </ul>
            </li>
            <li>
                <a href="personal-info.html">
                    <i class="icon-cog"></i>
                    <span>My Info</span>
                </a>
            </li>
            <li>
                <a class="dropdown-toggle" href="#">
                    <i class="icon-share-alt"></i>
                    <span>Extras</span>
                    <i class="icon-chevron-down"></i>
                </a>
                <ul class="submenu">
                    <li><a href="code-editor.html">Code editor</a></li>
                    <li><a href="grids.html">Grids</a></li>
                    <li><a href="signin.html">Sign in</a></li>
                    <li><a href="signup.html">Sign up</a></li>
                </ul>
            </li> -->
		</ul>
	</div>
	<!-- end sidebar -->

	<script type="text/javascript">
		$(function() {

			// jQuery Knobs
			$(".knob").knob();

			// jQuery UI Sliders
			$(".slider-sample1").slider({
				value : 100,
				min : 1,
				max : 500
			});
			$(".slider-sample2").slider({
				range : "min",
				value : 130,
				min : 1,
				max : 500
			});
			$(".slider-sample3").slider({
				range : true,
				min : 0,
				max : 500,
				values : [ 40, 170 ],
			});

			// jQuery Flot Chart
			var visits = [ [ 1, 50 ], [ 2, 40 ], [ 3, 45 ], [ 4, 23 ],
					[ 5, 55 ], [ 6, 65 ], [ 7, 61 ], [ 8, 70 ], [ 9, 65 ],
					[ 10, 75 ], [ 11, 57 ], [ 12, 59 ] ];
			var visitors = [ [ 1, 25 ], [ 2, 50 ], [ 3, 23 ], [ 4, 48 ],
					[ 5, 38 ], [ 6, 40 ], [ 7, 47 ], [ 8, 55 ], [ 9, 43 ],
					[ 10, 50 ], [ 11, 47 ], [ 12, 39 ] ];

			var plot = $.plot($("#statsChart"), [ {
				data : visits,
				label : "Signups"
			}, {
				data : visitors,
				label : "Visits"
			} ], {
				series : {
					lines : {
						show : true,
						lineWidth : 1,
						fill : true,
						fillColor : {
							colors : [ {
								opacity : 0.1
							}, {
								opacity : 0.13
							} ]
						}
					},
					points : {
						show : true,
						lineWidth : 2,
						radius : 3
					},
					shadowSize : 0,
					stack : true
				},
				grid : {
					hoverable : true,
					clickable : true,
					tickColor : "#f9f9f9",
					borderWidth : 0
				},
				legend : {
					// show: false
					labelBoxBorderColor : "#fff"
				},
				colors : [ "#a7b5c5", "#30a0eb" ],
				xaxis : {
					ticks : [ [ 1, "JAN" ], [ 2, "FEB" ], [ 3, "MAR" ],
							[ 4, "APR" ], [ 5, "MAY" ], [ 6, "JUN" ],
							[ 7, "JUL" ], [ 8, "AUG" ], [ 9, "SEP" ],
							[ 10, "OCT" ], [ 11, "NOV" ], [ 12, "DEC" ] ],
					font : {
						size : 12,
						family : "Open Sans, Arial",
						variant : "small-caps",
						color : "#697695"
					}
				},
				yaxis : {
					ticks : 3,
					tickDecimals : 0,
					font : {
						size : 12,
						color : "#9da3a9"
					}
				}
			});

			function showTooltip(x, y, contents) {
				$('<div id="tooltip">' + contents + '</div>').css({
					position : 'absolute',
					display : 'none',
					top : y - 30,
					left : x - 50,
					color : "#fff",
					padding : '2px 5px',
					'border-radius' : '6px',
					'background-color' : '#000',
					opacity : 0.80
				}).appendTo("body").fadeIn(200);
			}

			var previousPoint = null;
			$("#statsChart")
					.bind(
							"plothover",
							function(event, pos, item) {
								if (item) {
									if (previousPoint != item.dataIndex) {
										previousPoint = item.dataIndex;

										$("#tooltip").remove();
										var x = item.datapoint[0].toFixed(0), y = item.datapoint[1]
												.toFixed(0);

										var month = item.series.xaxis.ticks[item.dataIndex].label;

										showTooltip(item.pageX, item.pageY,
												item.series.label + " of "
														+ month + ": " + y);
									}
								} else {
									$("#tooltip").remove();
									previousPoint = null;
								}
							});
		});
	</script>

</body>
</html>