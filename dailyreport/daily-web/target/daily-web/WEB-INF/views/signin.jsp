<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html class="login-bg">
<head>
<title>昆明航空综合服务系统</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<!-- bootstrap -->
<link href="resources/css/bootstrap/bootstrap.css" rel="stylesheet" />
<link href="resources/css/bootstrap/bootstrap-responsive.css"
	rel="stylesheet" />
<link href="resources/css/bootstrap/bootstrap-overrides.css"
	type="text/css" rel="stylesheet" />

<!-- global styles -->
<link rel="stylesheet" type="text/css" href="resources/css/layout.css" />
<link rel="stylesheet" type="text/css" href="resources/css/elements.css" />
<link rel="stylesheet" type="text/css" href="resources/css/icons.css" />

<!-- libraries -->
<link rel="stylesheet" type="text/css"
	href="resources/css/lib/font-awesome.css" />

<!-- this page specific styles -->
<link rel="stylesheet" href="resources/css/compiled/signin.css"
	type="text/css" media="screen" />

<!-- open sans font -->
<link
	href='http://fonts.useso.com/css?family=Open+Sans:300italic,400italic,700italic,800italic,400,300,600,700,800'
	rel='stylesheet' type='text/css' />

<!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->


<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body onload='document.f.j_username.focus();'>
	<div class="row-fluid login-wrapper">
		<a href="resources/#"> <!--         <img class="logo" src="" /> -->
		</a>
		<div class="span4 box">
			<div class="content-wrap">
				<s:url var="authUrl" value="/usercheck" />
				<sf:form name="f" method="post" action="${authUrl}"
					modelAttribute="account">
					<fieldset>
						<h6>登录</h6>
						<sf:errors path="*" class="alert alert-error" />
						<div>
							<sf:input path="userId" id="username" class="span12"
								placeholder="请输入工号" />
						</div>
						<div>
							<sf:password path="passWord" id="password" class="span12"
								placeholder="默认密码a1234567" />
						</div>
						<div class="remember">
							<input id="remember-me" type="checkbox" /> <label
								for="remember-me">记住我</label> <a href="resources/#"
								class="forgot">忘记密码</a>
						</div>
						<%-- 	<div>
							<input type="hidden" name="${_csrf.parameterName}"
								value="${_csrf.token}" />
						</div> --%>
						<input class="btn-glow primary login" type="submit" name="submit"
							value="登录" />
					</fieldset>
				</sf:form>
			</div>
		</div>

		<!--     <div class="span4 no-account">
            <p>Don't have an account?</p>
            <a href="resources/signup.html">Sign up</a>
        </div> -->
	</div>
	<!-- scripts -->
	<script src="resources/js/jquery-latest.js"></script>
	<script src="resources/js/bootstrap.min.js"></script>
	<script src="resources/js/theme.js"></script>
</body>
</html>