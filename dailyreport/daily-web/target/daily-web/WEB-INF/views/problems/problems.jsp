<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>问题列表</title>
</head>
<body>
	这里是问题列表展示区 下面是你记录的问题<br>
	<c:forEach items="${problemList}" var="p">
	时间：${p.recordTime}  问题类型：${p.type}   问题描述：${p.description}  问题系统：${p.sourceSystem}  <br> 
	</c:forEach>
	<br>
	<sf:form>
		<input class="btn-glow primary login" type="submit" value="提交" />
	</sf:form>
	<sf:form method="post">
		<input class="btn-glow primary login" type="submit" value="新建" />
	</sf:form>
	<a href="problems/creat">问题列表</a>
</body>
</html>