<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<title>问题列表</title>

<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<!-- bootstrap -->
<link href="/daily-web/resources/css/bootstrap/bootstrap.css"
	rel="stylesheet" />
<link href="/daily-web/resources/css/bootstrap/bootstrap-responsive.css"
	rel="stylesheet" />
<link href="/daily-web/resources/css/bootstrap/bootstrap-overrides.css"
	type="text/css" rel="stylesheet" />

<!-- global styles -->
<link rel="stylesheet" type="text/css"
	href="/daily-web/resources/css/layout.css" />
<link rel="stylesheet" type="text/css"
	href="/daily-web/resources/css/elements.css" />
<link rel="stylesheet" type="text/css"
	href="/daily-web/resources/css/icons.css" />

<!-- libraries -->
<link href="/daily-web/resources/css/lib/font-awesome.css"
	type="text/css" rel="stylesheet" />

<!-- this page specific styles -->
<link rel="stylesheet"
	href="/daily-web/resources/css/compiled/tables.css" type="text/css"
	media="screen" />

<!-- open sans font -->
<link
	href='http://fonts.useso.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800'
	rel='stylesheet' type='text/css' />

<!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
	<!-- main container -->
	<div class="content">

		<!-- settings changer -->
		<div class="skins-nav">
			<a href="#" class="skin first_nav selected"> <span class="icon"></span><span
				class="text">Default</span>
			</a> <a href="#" class="skin second_nav" data-file="css/skins/dark.css">
				<span class="icon"></span><span class="text">Dark skin</span>
			</a>
		</div>

		<div class="container-fluid">
			<div id="pad-wrapper">

				<!-- products table-->
				<!-- the script for the toggle all checkboxes from header is located in js/theme.js -->
				<div class="table-wrapper products-table section">
					<div class="row-fluid head">
						<div class="span12">
							<h4>生产数据</h4>
						</div>
					</div>

					<div class="row-fluid filter-block">
						<div class="pull-right">
							<!-- 这是一个筛选框 -->
							<%--<div class="ui-select">
								<select>
									<option />Filter users
									<option />Signed last 30 days
									<option />Active users
								</select>
							</div> --%>
							<input type="text" class="search" /> <a
								class="btn-flat success new-product" href="problems/creat">+
								新建</a>
						</div>
					</div>

					<div class="row-fluid">
						<table class="table table-hover">
							<thead>
								<tr>
									<th class="span3"><input type="checkbox" /> 时间</th>
									<th class="span3"><span class="line"></span>问题类型</th>
									<th class="span3"><span class="line"></span>问题表述</th>
									<th class="span3"><span class="line"></span>所属系统</th>
									<th class="span1"><span class="line"></span>操作</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${problemList}" var="p">
									<!-- row -->
									<tr class="first">
										<td><input type="checkbox" /> <a href="#" class="name">${p.recordTime}</a></td>
										<td class="description">${p.type}</td>
										<td class="description">${p.description}</td>
										<td class="description">${p.sourceSystem}</td>
										<td class="align-left">
											<%--此为状态显示 <span class="label label-success">Active</span> --%>
											<ul class="action">
												<li><a href="#">修改</a></li>
												<li class="last"><a href="#">删除</a></li>
											</ul>
										</td>
									</tr>
								</c:forEach>

							</tbody>
						</table>
					</div>
				</div>
				<!-- end products table -->

			</div>
		</div>
	</div>
	<!-- end main container -->

	<!-- scripts -->
	<script src="/daily-web/resources/js/jquery-latest.js"></script>
	<script src="/daily-web/resources/js/bootstrap.min.js"></script>
	<script src="/daily-web/resources/js/theme.js"></script>

</body>
</html>