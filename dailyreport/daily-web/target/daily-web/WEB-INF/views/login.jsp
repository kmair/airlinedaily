<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html class="login-bg">
<head>
<title>昆明航空综合服务系统</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<!-- bootstrap -->
<link href="resources/css/bootstrap/bootstrap.css" rel="stylesheet" />
<link href="resources/css/bootstrap/bootstrap-responsive.css"
	rel="stylesheet" />
<link href="resources/css/bootstrap/bootstrap-overrides.css"
	type="text/css" rel="stylesheet" />

<!-- global styles -->
<link rel="stylesheet" type="text/css" href="resources/css/layout.css" />
<link rel="stylesheet" type="text/css" href="resources/css/elements.css" />
<link rel="stylesheet" type="text/css" href="resources/css/icons.css" />

<!-- libraries -->
<link rel="stylesheet" type="text/css"
	href="resources/css/lib/font-awesome.css" />

<!-- this page specific styles -->
<link rel="stylesheet" href="resources/css/compiled/signin.css"
	type="text/css" media="screen" />

<!-- open sans font -->
<link
	href='http://fonts.useso.com/css?family=Open+Sans:300italic,400italic,700italic,800italic,400,300,600,700,800'
	rel='stylesheet' type='text/css' />

<!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->


<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body onload='document.f.j_username.focus();'>
	<div class="row-fluid login-wrapper">
		<a href="resources/#"> <!--         <img class="logo" src="" /> -->
		</a>
		<div class="span4 box">
			<div class="content-wrap">
				<s:url var="authUrl" value="/j_spring_security_check" />
				<form action="${authUrl}" method="post">
					<h2>请先登录</h2>
					用户名：<input type="text" name="username"> <br> 密码：<input
						type="password" name="password"> <br> <input
						type="submit" name="submit" value="登陆">
				</form>
			</div>
		</div>

		<!--     <div class="span4 no-account">
            <p>Don't have an account?</p>
            <a href="resources/signup.html">Sign up</a>
        </div> -->
	</div>
	<!-- scripts -->
	<script src="resources/js/jquery-latest.js"></script>
	<script src="resources/js/bootstrap.min.js"></script>
	<script src="resources/js/theme.js"></script>
</body>
</html>