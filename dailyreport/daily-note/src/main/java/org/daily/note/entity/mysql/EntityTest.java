package org.daily.note.entity.mysql;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.daily.validation.entity.mysql.NoteEntityImpl;

@Entity
@Table(name = "test")
public class EntityTest extends NoteEntityImpl {
	@Column(name = "name")
	private String name;
	@Column(name = "notes")
	private String note;
	@Column(name = "price")
	private Integer price;

	public EntityTest() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

}
