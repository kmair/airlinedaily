package org.daily.note.entity.mysql;

import static org.junit.Assert.*;
import org.daily.dao.hibernate.dao.EntityOperationInface;
import org.daily.note.entity.mysql.Problems;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class NoteTest {
	private ApplicationContext act;
	private EntityOperationInface dao;

	@Before
	public void setUp() {
		act = new ClassPathXmlApplicationContext("org/daily/note/resources/daily-note-spring.xml");
		dao = (EntityOperationInface) act.getBean("mySQLOperate");
	}

	@Test
	public void getEntitTest() {
		Problems problem = (Problems) dao.getEntity(Problems.class, 1);
		assertEquals("test",problem.getType());
	}

}
