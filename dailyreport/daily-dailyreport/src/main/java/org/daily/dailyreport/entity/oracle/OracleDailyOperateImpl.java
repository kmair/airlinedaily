package org.daily.dailyreport.entity.oracle;

import java.util.Iterator;

import org.daily.dao.hibernate.datasources.HibernateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

@Service
public class OracleDailyOperateImpl implements OracleDailyOperateInterface {

	@Autowired
	@Qualifier("oracleDao")
	private HibernateService dao;

	public Iterator<Object> getEntity(String tableName) {
		String sql = "select * from " + tableName;
		Iterator it = dao.getCurrentSession().createSQLQuery(sql).list().iterator();
		return it;
	}

	public Iterator<Object> getEntity(String tableName, String querySql) {
		String sql = "select * from " + tableName + " where " + querySql;
		Iterator it = dao.getCurrentSession().createSQLQuery(sql).list().iterator();
		return it;
	}

	public Iterator<Object> getEntity(String tableName, String columnName, String queryValue) {
		String sql = "select * from " + tableName + " where " + columnName + " = " + queryValue;
		Iterator it = dao.getCurrentSession().createSQLQuery(sql).list().iterator();
		return it;
	}

	public Iterator<Object> getEntity(String tableName, String columnName, String beginValue, String endValue) {
		String sql = "select * from " + tableName + " where " + columnName + " between " + beginValue + " and "
				+ endValue;
		Iterator it = dao.getCurrentSession().createSQLQuery(sql).list().iterator();
		return it;
	}

	public void showResults(Iterator<Object> it) {
		while (it.hasNext()) {
			Object[] row = (Object[]) it.next();
			for (Object s : row) {
				System.out.print(s.toString() + " ");
			}
			System.out.println("\n");

		}
	}

	public static void main(String[] args) {
		ApplicationContext act;
		OracleDailyOperateImpl uinfo;

		act = new ClassPathXmlApplicationContext("org/daily/dailyreport/resources/daily-dailyreport-spring.xml");
		uinfo = (OracleDailyOperateImpl) act.getBean("oracleDailyOperateImpl");
		Iterator it = uinfo.getEntity(DUANXINSHUJU, "f_rq=to_date('2016-05-03','yyyy-mm-dd')");
		uinfo.showResults(it);
	}
}
