package org.daily.dailyreport.entity.oracle;

import java.util.Iterator;

public interface OracleDailyOperateInterface {
	public static final String DUANXINSHUJU = "vi_dxmbsj";

	public Iterator<Object> getEntity(String tableName);

	public Iterator<Object> getEntity(String tableName, String querySql);

	public Iterator<Object> getEntity(String tableName, String columnName, String queryValue);

	public Iterator<Object> getEntity(String tableName, String columnName, String beginValue, String endValue);

	public void showResults(Iterator<Object> it);
}
