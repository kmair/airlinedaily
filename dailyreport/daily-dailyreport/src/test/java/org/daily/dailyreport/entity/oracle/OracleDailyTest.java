package org.daily.dailyreport.entity.oracle;

import static org.junit.Assert.*;

import java.util.Iterator;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class OracleDailyTest {
	ApplicationContext act;
	OracleDailyOperateImpl uinfo;

	@Before
	public void setUp() throws Exception {
		act = new ClassPathXmlApplicationContext("org/daily/dailyreport/resources/daily-dailyreport-spring.xml");
		uinfo = (OracleDailyOperateImpl) act.getBean("oracleDailyOperateImpl");

	}

	@Test
	public void testShowResults() {
		Iterator<Object> it = uinfo.getEntity("user_info");
		assertTrue(it.hasNext());
	}

}
