package org.daily.dao.hibernate.dao;

import org.daily.dao.hibernate.dao.EntityOperationImpl;
import org.daily.dao.hibernate.datasources.HibernateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

@Repository
public  class OracleOperate extends EntityOperationImpl {
	@Autowired
	public OracleOperate(@Qualifier("oracleDao")HibernateService dao) {
		super(dao);
	}

}
