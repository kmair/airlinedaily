package org.daily.dao.hibernate.datasources;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 * 通过Hibernate连接数据的统一借口 现有两个实现类，连接MYSQL及连接Oracle数据库的SessionFactory
 * 
 * @author heye2
 *
 */
public interface HibernateService {
	public SessionFactory getSessionFactory();

	public Session  getCurrentSession();

}
