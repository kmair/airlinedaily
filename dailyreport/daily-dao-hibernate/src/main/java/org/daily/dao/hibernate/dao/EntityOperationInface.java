package org.daily.dao.hibernate.dao;


/**
 * 对数据据进行CURD操作
 * @author heye2
 *
 */
public interface EntityOperationInface {

	public <T> Object getEntity(Class<T> c, Integer i);

	public <T> Object getEntity(Class<T> c, String columName, String queryName);

	public <T> Object getOneEntity(Class<T> c, String columName, String queryName);

	public <T> Object getAllEntity(Class<T> c);

	public void SaveEntity(Object o);

	public void updateEntity(Object o);

	public void deleteEntity(Object o);

	public void saveOrUpdateEntity(Object o);

}
