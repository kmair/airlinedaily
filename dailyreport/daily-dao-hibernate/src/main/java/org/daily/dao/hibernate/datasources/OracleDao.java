package org.daily.dao.hibernate.datasources;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
/**
 * 链接Oracle数据库，库中包含日报数据
 * @author heye2
 *
 */
@Repository
public class OracleDao extends SessionFactoryImpl {
	
	@Autowired
	public OracleDao(@Qualifier("sessionFactoryOracle")SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}
}
