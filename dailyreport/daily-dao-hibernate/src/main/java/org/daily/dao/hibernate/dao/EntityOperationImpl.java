package org.daily.dao.hibernate.dao;

import java.util.List;

import org.daily.dao.hibernate.datasources.HibernateService;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

@Repository
public class EntityOperationImpl implements EntityOperationInface {
	
	private HibernateService dao;

	public EntityOperationImpl() {
	}

	// 根据需求，确定链接库
	public EntityOperationImpl(HibernateService dao) {
		this.dao = dao;
	}

	public <T> Object getEntity(Class<T> c, Integer i) {
		Session session = dao.getCurrentSession();
		T t = session.get(c, i);
		return t;
	}

	public void SaveEntity(Object o) {
		Session session = dao.getCurrentSession();
		session.save(o);
	}

	public void updateEntity(Object o) {
		// TODO Auto-generated method stub

	}

	public void deleteEntity(Object o) {
		// TODO Auto-generated method stub

	}

	public void saveOrUpdateEntity(Object o) {
		// TODO Auto-generated method stub

	}

	public HibernateService getDao() {
		return dao;
	}

	public <T> List<T> getAllEntity(Class<T> c) {
		Session session = dao.getCurrentSession();
		@SuppressWarnings("unchecked")
		List<T> t = session.createQuery("from " + c.getName()).list();
		return t;
	}

	// 分列名查询
	public <T> List<T> getEntity(Class<T> c, String columName, String queryName) {
		Session session = dao.getCurrentSession();
		@SuppressWarnings("unchecked")
		List<T> t = session.createQuery("from " + c.getName() + " as c where c." + columName + "=:queryName")
				.setString("queryName", queryName).list();
		return t;
	}

	public <T> Object getOneEntity(Class<T> c, String columName, String queryName) {
		Session session = dao.getCurrentSession();
		@SuppressWarnings("unchecked")
		T t = (T) session.createQuery("from " + c.getName() + " as c where c." + columName + "=:queryName")
				.setString("queryName", queryName).setMaxResults(1).uniqueResult();
		return t;
	}

}