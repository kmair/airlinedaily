package org.daily.dao.hibernate.datasources;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

public abstract class SessionFactoryImpl implements HibernateService {

	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

}
