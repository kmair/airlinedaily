package org.daily.dao.hibernate.datasources;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
/**
 * 连接Mysql，数据库为dailyreport，库中保存用户信息。
 * @author heye2
 *
 */
@Repository
public class MySQLDao extends SessionFactoryImpl {
	
	
	
	@Autowired
	public MySQLDao(@Qualifier("sessionFactoryMySQL")SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}
}
