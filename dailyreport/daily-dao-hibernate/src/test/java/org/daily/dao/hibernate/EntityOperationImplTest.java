/**
 * 
 */
package org.daily.dao.hibernate;

import static org.junit.Assert.*;

import org.daily.dao.hibernate.dao.EntityOperationImpl;
import org.daily.dao.hibernate.dao.EntityOperationInface;
import org.daily.dao.hibernate.datasources.HibernateService;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * 测试CRUD操作
 * 
 * @author heye2
 *
 */
public class EntityOperationImplTest {

	
	EntityOperationInface entityOperate;
	ApplicationContext ctx;
	EntityOperationInface eoi;

	@Before
	public void getBean() {
		ctx = new ClassPathXmlApplicationContext("org/daily/dao/hibernate/resources/spring-hibernate-datasources.xml");
		
		entityOperate = (EntityOperationInface) ctx.getBean("oracleOperate");
	}

	@Test
	public void testGetEntityClassOfTInteger() {
		assertFalse(entityOperate.toString().isEmpty());
	}


}
