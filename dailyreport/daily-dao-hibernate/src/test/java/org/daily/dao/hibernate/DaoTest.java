package org.daily.dao.hibernate;

import static org.junit.Assert.*;
import org.daily.dao.hibernate.dao.EntityOperationInface;
import org.daily.dao.hibernate.datasources.HibernateService;
import org.daily.dao.hibernate.datasources.OracleDao;
import org.hibernate.Session;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.junit.Before;
import org.junit.Test;

public class DaoTest {
	HibernateService mysqldao;
	HibernateService orqcledao;
	ApplicationContext ctx;
	EntityOperationInface eoi;

	public Object[] getResult() {
		String sql = "select * from user_info";
		Object[] row = (Object[]) orqcledao.getCurrentSession().createSQLQuery(sql).list().iterator().next();
		return row;
	}

	@Before
	public void getBean() {
		ctx = new ClassPathXmlApplicationContext("org/daily/dao/hibernate/resources/spring-hibernate-datasources.xml");
		mysqldao = (HibernateService) ctx.getBean("mySQLDao");
		orqcledao = (HibernateService) ctx.getBean("oracleDao");
	}

	@Test
	public void mysqlDaoTest() {
		Session session = mysqldao.getSessionFactory().openSession();
		String st = session.toString();
		assertFalse(st.isEmpty());
	}

	@Test
	public void orqcleDaoTest() {
		Session session = orqcledao.getSessionFactory().openSession();
		String st = session.toString();
		assertFalse(st.isEmpty());
	}

}
