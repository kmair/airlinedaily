package org.daily.validation.entity.mysql;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.springframework.stereotype.Component;

@Component
@Entity
@Table(name = "accounts")
public class Account extends NoteEntityImpl {

	@Pattern(regexp = "^[a-zA-Z0-9]+$", message = "用户名仅能包含字母与数字，不能有空格且不能为空。")
	@Column(name = "user_id")
	private String userId;
	@Column(name = "user_name")
	private String userName;
	@Size(min = 6, message = "密码长度不能少于6位")
	@Column(name = "password")	
	private String passWord;
	
	@Transient
	private Role roleId;

	public Account() {
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Role getRoleId() {
		return roleId;
	}

	public void setRoleId(Role roleId) {
		this.roleId = roleId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassWord() {
		return passWord;
	}

	public void setPassWord(String passWord) {
		this.passWord = passWord;
	}

}
