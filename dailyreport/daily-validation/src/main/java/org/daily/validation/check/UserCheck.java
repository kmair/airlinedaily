package org.daily.validation.check;

import java.util.Iterator;
import java.util.List;

import org.daily.dao.hibernate.dao.EntityOperationImpl;
import org.daily.dao.hibernate.dao.EntityOperationInface;
import org.daily.validation.entity.mysql.Account;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

@Service
public class UserCheck extends DataOperateServiceImpl {
	@Override
	public <T> boolean check(Class<T> c, String colmunName, String userName, String passWord) {

		boolean isTrue = false;
		@SuppressWarnings("unchecked")
		List<Account> results = (List<Account>) dao.getEntity(c, colmunName, userName);
		Iterator<Account> it = results.iterator();
		while (it.hasNext()) {
			Account account = it.next();
			if (passWord.equals(account.getPassWord())) {
				isTrue = true;
				return isTrue;
			}
		}
		return isTrue;
	}
}
