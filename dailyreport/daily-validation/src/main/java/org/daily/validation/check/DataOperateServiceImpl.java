package org.daily.validation.check;

import org.daily.dao.hibernate.dao.EntityOperationInface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public abstract class DataOperateServiceImpl implements DataOperateService {

	@Autowired
	@Qualifier("mySQLOperate")
	EntityOperationInface dao;

	public abstract <T> boolean check(Class<T> c, String colmunName, String userName, String passWord);


}
