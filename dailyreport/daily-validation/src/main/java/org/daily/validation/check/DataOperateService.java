package org.daily.validation.check;

import org.daily.dao.hibernate.dao.EntityOperationInface;

public interface DataOperateService {

	public <T> boolean check(Class<T> c, String colmunName, String userName, String passWord);

}
