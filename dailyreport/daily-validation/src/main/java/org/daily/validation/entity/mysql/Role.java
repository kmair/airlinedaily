package org.daily.validation.entity.mysql;

import java.util.Set;

import org.springframework.stereotype.Component;

@Component
public class Role extends NoteEntityImpl {
	private String roleName;
	private Integer parentId;
	private Set<Account> account;

	public Role() {
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	public Set<Account> getAccount() {
		return account;
	}

	public void setAccount(Set<Account> account) {
		this.account = account;
	}

}
