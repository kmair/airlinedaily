package org.daily.validation.check;

import static org.junit.Assert.*;

import org.daily.validation.entity.mysql.Account;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class UserCheckTest {
	UserCheck uc;
	ApplicationContext ctx;

	@Before
	public void setUp() throws Exception {
		ctx = new ClassPathXmlApplicationContext("org/daily/validation/resources/daily-validation-spring.xml");
		uc = (UserCheck) ctx.getBean("userCheck");
	}

	@Test
	public void testCheckUser() {
		assertTrue(uc.check(Account.class, "userId", "a10001", "a10001"));
	}

}
